__description__ = "a module that tests segment utilities in utils.py"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

# --------------------------------------------------------------------------

import pytest

import numpy as np

from ligo import segments

from idq import utils

# --------------------------------------------------------------------------

# ----------------------
#        tests
# ----------------------


@pytest.mark.usefixtures("test_segments")
class TestSegmentKFold(object):
    """
    Tests the different k-fold segment functions used for splitting segments by
    livetime.
    """

    def test_split_segments_by_livetime(self):
        num_splits = 5
        livetime1 = float(utils.livetime(self.seglist_single))
        split1 = utils.split_segments_by_livetime(self.seglist_single, num_splits)
        split_livetime1 = [utils.livetime(seg) for seg in split1]

        assert (
            sum(split_livetime1) == livetime1
        ), "sum of split livetime and original livetime do not match"
        assert (
            len(split1) == num_splits
        ), "split_segments_by_livetime did not return the right number of splits"
        for lt in split_livetime1:
            assert np.isclose(
                lt, livetime1 / num_splits, rtol=0.01
            ), "livetime for a particular split is not within tolerance"

        livetime2 = float(utils.livetime(self.seglist_multiple))
        split2 = utils.split_segments_by_livetime(self.seglist_multiple, num_splits)
        split_livetime2 = [utils.livetime(seg) for seg in split2]

        assert (
            sum(split_livetime2) == livetime2
        ), "sum of split livetime and original livetime do not match"
        assert (
            len(split1) == num_splits
        ), "split_segments_by_livetime did not return the right number of splits"
        for lt in split_livetime2:
            assert np.isclose(
                lt, livetime2 / num_splits, rtol=0.01
            ), "livetime for a particular split is not within tolerance"

    def test_segments_kfold(self):
        num_splits = 5
        livetime1 = float(utils.livetime(self.seglist_single))
        kfold1 = utils.segments_kfold(self.seglist_single, num_splits)

        all_folds1 = segments.segmentlist([])
        for rest, this in kfold1:
            all_folds1 += this
            assert np.isclose(
                utils.livetime(this), livetime1 / num_splits, rtol=0.01
            ), "livetime for a particular split is not within tolerance"
            assert np.isclose(
                utils.livetime(rest),
                (livetime1 * (num_splits - 1)) / num_splits,
                rtol=0.01,
            ), "livetime for a particular split is not within tolerance"
            assert (
                rest + this
            ) == self.seglist_single, (
                "combining the folds does not return the original segment list"
            )
        assert (
            all_folds1 == self.seglist_single
        ), "combining all the single folds does not return the original segment list"

        livetime2 = float(utils.livetime(self.seglist_multiple))
        kfold2 = utils.segments_kfold(self.seglist_multiple, num_splits)

        all_folds2 = segments.segmentlist([])
        for rest, this in kfold2:
            all_folds2 += this
            assert np.isclose(
                utils.livetime(this), livetime2 / num_splits, rtol=0.01
            ), "livetime for a particular split is not within tolerance"
            assert np.isclose(
                utils.livetime(rest),
                (livetime2 * (num_splits - 1)) / num_splits,
                rtol=0.01,
            ), "livetime for a particular split is not within tolerance"
            assert (
                rest + this
            ) == self.seglist_multiple, (
                "combining the folds does not return the original segment list"
            )
        assert (
            all_folds2 == self.seglist_multiple
        ), "combining all the single folds does not return the original segment list"

    def test_segments_causal_kfold(self):
        num_splits = 5
        livetime1 = float(utils.livetime(self.seglist_single))
        kfold1 = utils.segments_causal_kfold(self.seglist_single, num_splits)

        all_folds1 = segments.segmentlist([])
        for ii, (train, evaluate) in enumerate(kfold1):
            if ii == 0:
                all_folds1 += train
            all_folds1 += evaluate
            assert np.isclose(
                utils.livetime(train), (livetime1 * ii) / (num_splits), rtol=0.01
            ), "livetime for a train split is not within tolerance"
            assert np.isclose(
                utils.livetime(evaluate), livetime1 / (num_splits), rtol=0.01
            ), "livetime for an evaluate split is not within tolerance"
        assert (
            all_folds1 == self.seglist_single
        ), "combining all the single folds does not return the original segment list"

        livetime2 = float(utils.livetime(self.seglist_multiple))
        kfold2 = utils.segments_causal_kfold(self.seglist_multiple, num_splits)

        all_folds2 = segments.segmentlist([])
        for ii, (train, evaluate) in enumerate(kfold2):
            if ii == 0:
                all_folds2 += train
            all_folds2 += evaluate
            assert np.isclose(
                utils.livetime(train), (livetime2 * ii) / (num_splits), rtol=0.01
            ), "livetime for a train split is not within tolerance"
            assert np.isclose(
                utils.livetime(evaluate), livetime2 / (num_splits), rtol=0.01
            ), "livetime for an evaluate split is not within tolerance"
        assert (
            all_folds2 == self.seglist_multiple
        ), "combining all the single folds does not return the original segment list"
