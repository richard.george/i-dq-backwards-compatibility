from collections import defaultdict
import logging
import os
import time

from gpstime import gpstime
import numpy as np

from gwpy.segments import DataQualityFlag
from gwpy.timeseries import TimeSeries
from lal.gpstime import tconvert
from ligo.scald import report
from ligo.segments import segment, segmentlist

from . import __version__
from . import configparser
from . import exceptions
from . import plots
from . import names
from . import features
from . import factories
from . import utils
from .series import combine_series, extract_segments_from_series

__process_name__ = "iDQ"

logger = logging.getLogger("idq")

DEFAULT_LOGLIKE_THRESHOLD = 5
DEFAULT_FIGTYPE = "png"
DEFAULT_LINKS = [
    ("https://git.ligo.org/lscsoft/iDQ", "iDQ source code"),
    ("https://git.ligo.org/lscsoft/iDQ/wikis", "iDQ wiki"),
    ("https://docs.ligo.org/lscsoft/iDQ/", "iDQ docs"),
]


class Report(object):
    """Generate everything needed for a report."""

    def __init__(
        self,
        config_path,
        start,
        end,
        segments=None,
        t0=None,
        zoom_start=None,
        zoom_end=None,
        skip_timeseries=False,
    ):
        self._start = start
        self._end = end
        self._t0 = t0
        self._zoom_start = zoom_start
        self._zoom_end = zoom_end

        if segments:
            self._segments = segments
        else:
            self._segments = segmentlist([segment(start, end)])

        # load configuration
        self._config_path = os.path.abspath(os.path.realpath(config_path))
        self._config = configparser.path2config(config_path)
        self._instrument = self.config.instrument
        self._tag = self.config.tag

        self.figtype = DEFAULT_FIGTYPE
        self.loglike_thr = DEFAULT_LOGLIKE_THRESHOLD
        self.skip_timeseries = skip_timeseries

        self.legend = self._config.report.get("legend", True)
        self.annotate_gch = self._config.report.get("annotate_gch", False)
        self.annotate_auc = self._config.report.get("annotate_auc", False)
        self.single_calib_plots = self._config.report.get("single_calib_plots", True)
        self.rank_roc = self._config.report.get("rank_roc", False)
        self.classifier_page = self._config.report.get("classifier_page", True)
        self.event_page = self._config.report.get("event_page", True)
        self.veto_summary = self._config.report.get("veto_summary", True)
        self.plot_qscan = self._config.report.get("plot_qscan", True)
        self.plot_map_coverage = self._config.report.get("plot_map_coverage", False)
        self.symlink_sum_pages = self._config.report.get("symlink_sum_pages", True)

        # check frequency bounds
        if "frequency" in self.config.features:
            target_bounds = configparser.config2bounds(
                self.config.samples["target_bounds"]
            )
            frequency = self.config.features["frequency"]
            assert (
                frequency in target_bounds
            ), f"must specify a frequency range ({frequency}) within target_bounds"
            freq_min, freq_max = target_bounds[frequency]
            assert isinstance(freq_min, int) and isinstance(
                freq_max, int
            ), "frequency bounds must be integers!"
            self._freq_bounds = freq_min, freq_max
        else:
            self._freq_bounds = None

        # time for report creation
        self.time_created = time.localtime()

    @property
    def config(self):
        return self._config

    @property
    def config_path(self):
        return self._config_path

    @property
    def instrument(self):
        return self._instrument

    @property
    def start(self):
        return self._start

    @property
    def end(self):
        return self._end

    @property
    def segs(self):
        return self._segments

    @property
    def zoom_start(self):
        return self._zoom_start if (self._zoom_start is not None) else self._start

    @property
    def zoom_end(self):
        return self._zoom_end if (self._zoom_end is not None) else self._end

    @property
    def t0(self):
        return (
            self._t0
            if (self._t0 is not None)
            else 0.5 * (self.zoom_start + self.zoom_end)
        )

    @property
    def tag(self):
        return self._tag

    def create_runtime_overview(self):
        """Generate runtime statistics and return a table element for the report."""
        start_date = tconvert(self.start)
        end_date = tconvert(self.end)
        livetime = utils.livetime(self.segs)
        columns = ["", ""]
        rows = [
            ["Start Time", f"{self.start:.3f} ({start_date})"],
            ["End Time", f"{self.end:.3f} ({end_date})"],
            ["Livetime (sec)", f"{livetime:.1f}"],
            [
                "Detector Duty Cycle",
                f"{(100 * livetime) / (self.end - self.start):.1f} %",
            ],
        ]
        return report.FlatTable("Summary", columns=columns, rows=rows)

    def create_classifier_overview(
        self,
        nicknames,
        datasets,
        models,
        calibmaps,
        series=None,
    ):
        """Generate classifier overview statistics and return a table element
        for the report."""
        columns = [
            "nickname",
            "No. glitch samples",
            "No. clean samples",
            "No. models",
            "No. calibration maps",
        ]
        if series is not None:
            columns += [
                f"Time with log-likelihood >= {self.loglike_thr:.1f}",
                "Walltime Duty Cycle",
                "Science Duty Cycle",
            ]

        rows = []
        for nickname in nicknames:
            # do some quick data manipulations
            gch, cln = datasets[nickname].vectors2classes()
            row = [
                nickname,
                "%d" % len(gch),
                "%d" % len(cln),
                "%d" % len(models[nickname]),
                "%d" % len(calibmaps[nickname]),
            ]

            if series is not None:
                times = np.concatenate([s.times for s in series[nickname]])
                loglike = np.concatenate([s.loglike.series for s in series[nickname]])
                loglike = loglike[(self.start <= times) * (times < self.end)]
                loglike_frac = np.sum(loglike >= self.loglike_thr) / len(loglike)

                row.append(f"{100. * loglike_frac:.3f} %")
                these_segs = extract_segments_from_series(series[nickname])
                # series might slightly over-cover requested segments
                walltime_duty_cycle = 100 * min(
                    1.0, utils.livetime(these_segs) / (self.end - self.start)
                )
                science_duty_cycle = (
                    100
                    * utils.livetime(these_segs & self.segs)
                    / utils.livetime(self.segs)
                )
                row.append(f"{walltime_duty_cycle:.2f} %")
                row.append(f"{science_duty_cycle:.2f} %")

            rows.append(row)

        return report.FlatTable("Basic Statistics", columns=columns, rows=rows)

    def create_veto_summary(self, nickname, datasets, series, segments):
        """Generate veto summary statistics into a table."""
        columns = ["Metric", "Threshold", "Veto Efficiency", "Deadtime"]
        rows = []

        veto_thresholds = self.config.report["veto_thresholds"]
        padding = 0.25
        livetime = utils.livetime(segments)
        glitch, _ = datasets[nickname].vectors2classes()
        for metric, thresholds in veto_thresholds.items():
            for threshold in thresholds:
                veto = DataQualityFlag()
                for s in series[nickname]:
                    metric_series = TimeSeries(
                        getattr(s, metric).series, t0=s.t0, dt=s.dt
                    )
                    if metric == "fap":
                        veto |= (metric_series <= threshold).to_dqflag()
                    else:
                        veto |= (metric_series >= threshold).to_dqflag()
                veto = veto.pad(-padding, padding)
                in_veto = utils.times_in_segments(glitch.times, veto.active)

                # calculate metrics
                efficiency = float(sum(in_veto) / len(glitch.times))
                deadtime = veto.livetime
                frac_deadtime = float(deadtime / livetime)
                veto_eff = efficiency / frac_deadtime

                row = [
                    metric,
                    threshold,
                    round(veto_eff, 3),
                    f"{round(100.0 * frac_deadtime, 3)} %",
                ]
                rows.append(row)

        return [
            report.FlatTable("Summary", columns=columns, rows=rows),
            report.Description(
                f"All vetoes have {padding:.2f}s padding applied "
                "to the start and end of each segment."
            ),
        ]

    def create_metadata_footer(self):
        metadata = []
        metadata.append(report.Footer("Report Metadata"))
        metadata.append(
            report.Description(
                "generated at {}".format(
                    time.strftime("%H:%M:%S %Z %a %d %b %Y", self.time_created)
                )
            )
        )
        metadata.append(report.Description(f"iDQ version : {__version__}"))
        metadata.append(
            report.Description(f"username : {os.environ.get('USER', 'unknown')}")
        )
        metadata.append(
            report.Description(f"hostname : {os.environ.get('HOSTNAME', 'unknown')}")
        )
        return metadata

    def report(self, nicknames, reportdir):
        """Generate a full report."""
        # act like a DiskReporter...
        if self.symlink_sum_pages:
            sum_start = gpstime.fromgps(self.start).strftime("%Y%m%d")
            symlinkdir = os.path.join(reportdir, f"summary_pages/{sum_start}")
            os.makedirs(os.path.join(reportdir, "summary_pages"), exist_ok=True)
        reportdir = names.start_end2fragmented_dir(
            self.start, self.end, rootdir=reportdir
        )

        os.makedirs(reportdir, exist_ok=True)

        logger.info(
            "generating report for %s within [%.3f, %.3f) with %s"
            % (", ".join(nicknames), self.start, self.end, self.config_path)
        )

        # find timeseries, models and calibration maps
        models = {}
        calibmaps = {}

        # just grab everything available
        if self.skip_timeseries:
            raise NotImplementedError("--skip-timeseries not currently supported")

        # grab series and only load the ones used to generate this data
        else:
            logger.info(
                "finding timeseries within [%.3f, %.3f)" % (self.start, self.end)
            )
            series = {}
            for nickname in nicknames:
                series[nickname] = find_timeseries(
                    self.config, self.start, self.end, nickname
                )
                logger.info(
                    "found %d timeseries for %s" % (len(series[nickname]), nickname)
                )

            model_hashes = {
                s.model_id for nickname in nicknames for s in series[nickname]
            }
            data_start = min(names.hash2start_end(hash_)[0] for hash_ in model_hashes)

            # find calibration maps
            logger.info(
                "finding calibration maps within [%.3f, %.3f) based on timeseries"
                % (data_start, self.end)
            )
            for nickname in nicknames:
                calibration_ids = {s.calibration_id for s in series[nickname]}
                unique_ids = {names.hash2id(calib_id) for calib_id in calibration_ids}
                calibmaps[nickname] = find_calibration_maps(
                    self.config, data_start, self.end, nickname, hashes=calibration_ids
                )
                logger.info(
                    "found %d calibration maps out of %d requested for %s"
                    % (len(calibmaps[nickname]), len(unique_ids), nickname)
                )

            # find models
            logger.info(
                "finding models within [%.3f, %.3f) based on timeseries"
                % (data_start, self.end)
            )
            for nickname in nicknames:
                model_ids = {s.model_id for s in series[nickname]}
                models[nickname] = find_models(
                    self.config, data_start, self.end, nickname, hashes=model_ids
                )
                logger.info(
                    "found %d models out of %d requested for %s"
                    % (len(models[nickname]), len(model_ids), nickname)
                )

            # determine spans for each calibration ID
            _, maps_by_span = find_span_ids(series)

            # find datasets for each calibration ID based on their spans
            logger.info("finding evaluated datasets covering timeseries analyzed")
            datasets = defaultdict(dict)
            for nickname in nicknames:
                for calib_id, segments in maps_by_span[nickname].items():
                    datasets[nickname][calib_id] = find_datasets(
                        self.config,
                        self.start,
                        self.end,
                        nickname,
                        segments=segments,
                        data_id=calib_id,
                    )

            # combine the datasets together
            for nickname in nicknames:
                dataset = features.Dataset(start=self.start, end=self.end)
                for dset in datasets[nickname].values():
                    dataset += dset

                # attach a dataloader to enable loading features as requested
                # (feature importance plots, etc)
                # FIXME: this is done in a roundabout way but this should be
                # refactored to not use private variables
                dataloader_factory = factories.DataLoaderFactory()
                dataloader = dataloader_factory(
                    dataset.start,
                    dataset.end,
                    segs=dataset.segs,
                    **self.config.features,
                )
                dataset._dataloader = dataloader

                datasets[nickname] = dataset

            for nickname in nicknames:
                gch, cln = datasets[nickname].vectors2classes()
                logger.info(
                    "found %d gch samples and %d cln samples for %s"
                    % (len(gch), len(cln), nickname)
                )

        # instantiate the report
        report_name = "%s Report at %s (%s)" % (
            __process_name__,
            self.instrument,
            self.tag,
        )
        thisreport = report.Report(
            report_name, theme="gwdetchar", detector=self.instrument
        )
        summary = report.Tab("Summary")

        logger.info("computing runtime overview")
        summary += self.create_runtime_overview()

        # generate a table summarizing the basic numbers for each nickname
        logger.info("computing run statistics")
        summary += report.Header("Run Statistics")
        summary += self.create_classifier_overview(
            nicknames,
            datasets,
            models,
            calibmaps,
            series=series,
        )

        # make plots
        # ROC curve
        logger.info("generating ROC plot")
        title = "Receiver Operating Characteristic (ROC) Curves"
        path = plot_roc(
            self.config,
            self.start,
            self.end,
            nicknames,
            datasets,
            series=series,
            output_dir=reportdir,
            annotate_auc=self.annotate_auc,
            rank_roc=self.rank_roc,
        )
        logger.debug(f"saved plot to: {path}")
        summary += report.Header(title)
        summary += report.ImageBoard(title, [[path]])

        # series and calib stuff that depends on series
        calib_plots = []
        if series is not None:
            # timeseries
            logger.info("generating histograms")
            title = "Timeseries Histograms"
            path = plot_histogram(
                self.config,
                self.start,
                self.end,
                nicknames,
                series,
                datasets=datasets,
                output_dir=reportdir,
            )
            logger.debug(f"saved plot to: {path}")
            summary += report.Header(title)
            summary += report.ImageBoard(title, [[path]])

            # FIXME: add cumulative histograms of timeseries behavior
            summary += report.Header("Calibration")

            # calibration accuracy
            logger.info("generating calibration accuracy plot")
            path = plot_calibration_accuracy(
                self.config,
                self.start,
                self.end,
                nicknames,
                calibmaps,
                series,
                datasets,
                output_dir=reportdir,
                plot_map_coverage=self.plot_map_coverage,
            )
            summary += report.ImageBoard("Calibration", [[path]])
            logger.debug(f"saved plot to: {path}")

            # calibration coverage
            logger.info("generating calibration coverage plot")
            path = plot_calibration_coverage(
                self.config,
                self.start,
                self.end,
                nicknames,
                series,
                datasets,
                calibmaps,
                output_dir=reportdir,
                plot_map_coverage=self.plot_map_coverage,
            )
            calib_plots.append(path)
            logger.debug(f"saved plot to: {path}")

        # calibration distributions
        logger.info("generating calibration pdf/cdf plot")
        path = plot_calibration_pdf_cdf(
            self.config,
            self.start,
            self.end,
            nicknames,
            calibmaps,
            output_dir=reportdir,
        )
        calib_plots.append(path)
        logger.debug(f"saved plot to: {path}")

        summary += report.ImageBoard("Calibration", [calib_plots])

        # add correlation plots
        if len(nicknames) > 1:
            logger.info("generating glitch/clean corner plots")
            gch_path, cln_path = plot_corner_glitch_clean(
                self.config,
                self.start,
                self.end,
                nicknames,
                datasets,
                output_dir=reportdir,
            )
            logger.debug(f"saved plots to: {gch_path}, {cln_path}")
            summary += report.Header("Comparisons")
            summary += report.ImageBoard("Comparisons", [gch_path, cln_path])

            # FIXME: add a corner for series as well?

        # write metadata
        summary += self.create_metadata_footer()
        thisreport += summary

        # generate single-classifier summary pages
        if self.classifier_page:
            logger.info("generating single-classifier summary pages")
            comparisons = self.classifier_report(
                nicknames,
                datasets,
                models,
                calibmaps,
                series=series,
                output_dir=reportdir,
            )
            for comparison in comparisons:
                thisreport += comparison

        if self.event_page:
            logger.info("generating event pages")
            try:
                trigger_target_times = find_target_features(
                    self.start, self.end, self.config, self.segs, logger, n_events=10
                )
            except exceptions.NoDataError:
                logger.info("no data, skipping summary of loudest trigger events")
            else:
                logger.info("generating summary of loudest trigger events")
                trigger_events = self.event_report(
                    nicknames,
                    datasets,
                    models,
                    calibmaps,
                    self.segs,
                    series=series,
                    logger=logger,
                    output_dir=reportdir,
                    times=trigger_target_times,
                    page="Loudest Trigger Events",
                )
                thisreport += trigger_events

            logger.info("generating summary of loudest iDQ events")
            if series is not None:
                logl_target_times = find_loudest_times(nicknames, series, n_events=10)
                for nickname in nicknames:
                    events = self.event_report(
                        [nickname],
                        datasets,
                        models,
                        calibmaps,
                        self.segs,
                        series=series,
                        logger=logger,
                        output_dir=reportdir,
                        times=logl_target_times[nickname],
                        page=f"Loudest {nickname} Events",
                    )
                    thisreport += events

        # write html page
        logger.info("writing report to {}".format(reportdir))
        thisreport.save(reportdir, config_name="report_test")

        # symlink the summary page
        if self.symlink_sum_pages:
            sum_start = gpstime.fromgps(self.end).strftime("%H:%M:%S")
            sum_end = gpstime.fromgps(self.end).strftime("%H:%M:%S")
            if sum_start == "00:00:00" and sum_end == "00:00:00":
                os.symlink(reportdir, symlinkdir)

    def classifier_report(
        self,
        nicknames,
        datasets,
        models,
        calibmaps,
        series=None,
        output_dir=".",
    ):
        """
        generate single-classifier summary information
        """
        comparisons = []
        for nickname in nicknames:
            logger.info("generating single-classifier HTML page for " + nickname)
            comparison = report.Tab(nickname)

            logger.info("computing runtime overview")
            comparison += self.create_runtime_overview()

            # generate a table summarizing the basic numbers for each nickname
            logger.info("computing basic statistics")
            comparison += report.Header("Basic Statistics")
            comparison += self.create_classifier_overview(
                [nickname],
                datasets,
                models,
                calibmaps,
                series=series,
            )

            # generate a table summarizing veto efficiencies
            if self.veto_summary and "veto_thresholds" in self.config.report:
                logger.info("computing veto statistics")
                comparison += report.Header("Veto Statistics")
                comparison += self.create_veto_summary(
                    nickname, datasets, series, self.segs
                )

            # make plots
            # ROC curve
            logger.info("generating ROC plot")
            path = plot_roc(
                self.config,
                self.start,
                self.end,
                [nickname],
                datasets,
                series=series,
                output_dir=output_dir,
                annotate_auc=self.annotate_auc,
            )
            logger.debug(f"saved plot to: {path}")
            title = "Receiver Operating Characteristic (ROC) Curves"
            comparison += report.Header(title)
            comparison += report.ImageBoard(title, images=[[path]])

            # series and calib stuff that depends on series
            calib_plots = []
            if series is not None:
                # timeseries
                logger.info("generating histograms")
                title = "Timeseries Histograms"
                path = plot_histogram(
                    self.config,
                    self.start,
                    self.end,
                    [nickname],
                    series,
                    datasets=datasets,
                    output_dir=output_dir,
                )
                logger.debug(f"saved plot to: {path}")
                comparison += report.Header(title)
                comparison += report.ImageBoard(title, [[path]])

                # FIXME: add cumulative histograms of timeseries behavior

                # calibration coverage
                logger.info("generating calibration coverage plot")
                path = plot_calibration_coverage(
                    self.config,
                    self.start,
                    self.end,
                    [nickname],
                    series,
                    datasets,
                    calibmaps,
                    output_dir=output_dir,
                    plot_map_coverage=self.plot_map_coverage,
                )
                logger.debug(f"saved plot to: {path}")
                calib_plots.append(path)

            # calibration distributions
            logger.info("generating calibration pdf/cdf plot")
            path = plot_calibration_pdf_cdf(
                self.config,
                self.start,
                self.end,
                [nickname],
                calibmaps,
                output_dir=output_dir,
            )
            logger.debug(f"saved plot to: {path}")
            calib_plots.append(path)

            comparison += report.Header("Calibration")
            comparison += report.ImageBoard("Calibration", [calib_plots])

            # individual calibration distribs
            if self.single_calib_plots:
                logger.info(
                    "generating calibration plots for individual calibration maps"
                )
                if series is not None:
                    _, calibs_segdict = find_span_ids(series)
                else:
                    calibs_segdict = None

                for calibmap in calibmaps[nickname].values():
                    # add a section to calibration for each individual map
                    calib_name = f"{nickname}_{calibmap.hash}"

                    # calibration coverage for only the samples evaluated with
                    # this map
                    if series is not None:
                        dataset = datasets[nickname]
                        if calibs_segdict is not None:
                            # filter this to only contain times when which this
                            # calibmap was used
                            dataset = dataset.copy()
                            dataset.filter(
                                segs=(
                                    dataset.segs
                                    & calibs_segdict[nickname][calibmap.hash]
                                )
                            )

                        logger.info(f"generating coverage plot for ID: {calibmap.hash}")
                        coverage_path = plot_calibration_coverage(
                            self.config,
                            self.start,
                            self.end,
                            [calib_name],
                            {calib_name: series[nickname]},
                            {calib_name: dataset},
                            output_dir=output_dir,
                            calib_id=calibmap.hash,
                            plot_map_coverage=self.plot_map_coverage,
                        )
                        logger.debug(f"saved plot to: {path}")
                    else:
                        coverage_path = None

                    # distribution showing only this map
                    logger.info(f"generating pdf/cdf plot for ID: {calibmap.hash}")
                    distrib_path = plot_calibration_pdf_cdf(
                        self.config,
                        self.start,
                        self.end,
                        [calib_name],
                        {calib_name: {calibmap.hash: calibmap}},
                        output_dir=output_dir,
                        calib_id=calibmap.hash,
                    )
                    logger.debug(f"saved plot to: {path}")

                    thisgrid = report.ImageGrid(
                        nickname + calibmap.hash, grid_size=2, visible=False
                    )
                    if coverage_path:
                        thisgrid += report.Image(path=coverage_path)
                    thisgrid += report.Image(path=distrib_path)
                    comparison += thisgrid

            # feature importance
            comparison += report.Header("Feature Importance")
            for i, (model_hash, model) in enumerate(models[nickname].items(), 1):
                logger.info(
                    "generating {}/{} feature importance plots".format(
                        i, len(models[nickname])
                    )
                )
                try:
                    importance_paths, importance_cols_datas = plot_feature_importance(
                        self.config,
                        self.start,
                        self.end,
                        [nickname],
                        {nickname: {model_hash: model}},
                        datasets=datasets,
                        series=series,
                        t0=self.t0,
                        output_dir=output_dir,
                    )

                    for (model_name, path), (_, cols, data) in zip(
                        importance_paths, importance_cols_datas
                    ):
                        # feature importance plots
                        if path is not None:
                            thisgrid = report.ImageGrid(
                                model_name, grid_size=2, visible=False
                            )
                            thisgrid += report.Image(path=path)
                            comparison += thisgrid

                        # veto configurations
                        if cols is not None:
                            comparison += report.FlatTable(
                                f"Veto Config: {model_name}", columns=cols, rows=data
                            )
                except exceptions.NoDataError:
                    logger.info("no data, skipping feature importance plot")

            # add footer
            comparison += self.create_metadata_footer()
            comparisons.append(comparison)

        return comparisons

    def event_report(
        self,
        nicknames,
        datasets,
        models,
        calibmaps,
        segs,
        times,
        series=None,
        logger=None,
        output_dir=".",
        page="Events",
    ):
        """
        generate single-event summary information
        """
        if logger is not None:
            logger.info("generating top significance events HTML page")
        events = report.Tab(page)

        title = "Timeseries"
        events += report.Header(title)

        if series is not None:
            for trigger_time in times:
                window_start = trigger_time - 2.0
                window_end = trigger_time + 2.0
                path = plot_timeseries(
                    self.config,
                    window_start,
                    window_end,
                    nicknames,
                    series,
                    t0=trigger_time,
                    datasets=datasets,
                    segments=self.segs,
                    plot_qscan=self.plot_qscan,
                    output_dir=output_dir,
                )
                title = f"{trigger_time} Timeseries"
                events += report.ImageBoard(title, [[path]])

            # add footer
            events += self.create_metadata_footer()

        return events


def find_timeseries(config, start, end, nickname):
    """Find all timeseries within start/end corresponding to a single nickname."""
    reporter_factory = factories.ReporterFactory()
    timeseriesdir = names.tag2timeseriesdir(config.tag, rootdir=config.rootdir)
    timeseriesreporter = reporter_factory(
        timeseriesdir, start, end, **config.timeseries["reporting"]
    )

    all_series = []
    for _, _, found_series in timeseriesreporter.glob(
        f"{config.instrument}-{nickname}", start, end
    ):
        for series in found_series:
            series.crop(start, end)
            if len(series):  # some remaining time
                all_series.append(series)

    # backwards compatibility with older (pre v0.5) timeseries
    if not all_series:
        for _, _, found_series in timeseriesreporter.glob(
            f"{config.instrument[0]}-{nickname}timeseries", start, end
        ):
            for series in found_series:
                series.crop(start, end)
                if len(series):  # some remaining time
                    all_series.append(series)

    return combine_series(all_series)


def find_datasets(config, start, end, nickname, segments=None, data_id=None):
    """Find all datasets within start/end corresponding to a single nickname."""
    if data_id is None:
        data_id = "*"

    reporter_factory = factories.ReporterFactory()
    evaluatedir = names.tag2evaluatedir(config.tag, rootdir=config.rootdir)
    evaluatereporter = reporter_factory(
        evaluatedir, start, end, **config.evaluate["reporting"]
    )

    datasets = features.Dataset(start=start, end=end, segs=segments)
    segs = segmentlist([segment(start, end)])
    if segments:
        segs &= segments

    for _, _, dataset in evaluatereporter.glob(nickname, start, end, data_id=data_id):
        # trim to requested start/end times
        dataset.filter(segs=(dataset.segs & segs))
        if len(dataset) > 0:
            datasets += dataset

    # backwards compatibility with older (pre v0.5) datasets
    if len(datasets) == 0:
        name = f"{nickname}evaluate"
        for _, _, dataset in evaluatereporter.glob(name, start, end, data_id=None):
            # trim to requested start/end times
            dataset.filter(segs=(dataset.segs & segs))
            if len(dataset) > 0:
                datasets += dataset

    datasets.filter()
    return datasets


def find_models(config, start, end, nickname, hashes=None):
    """Find all classifier models within start/end corresponding to a single
    nickname."""
    reporter_factory = factories.ReporterFactory()
    traindir = names.tag2traindir(config.tag, rootdir=config.rootdir)
    trainreporter = reporter_factory(traindir, start, end, **config.train["reporting"])

    models = {}
    for _, _, model in trainreporter.glob(nickname, start, end, data_id="*"):
        models[model.hash] = model

    # backwards compatibility with older (pre v0.5) models
    if not models:
        name = f"{nickname}train"
        for _, _, model in trainreporter.glob(name, start, end, data_id=None):
            models[model.hash] = model

    if hashes is not None:
        models = filter_data_by_hash(models, hashes)
    return models


def reduce_maps_by_model_id(data):
    """Limit data to contain latest map per model id"""
    calibmap_id = {}
    calib_maps = {}
    for hash_, d in data.items():
        id_ = d.model_id
        if id_ not in calibmap_id:
            calibmap_id[id_] = (hash_, d)
        elif d.end > calibmap_id[id_][1].end:
            calibmap_id[id_] = (hash_, d)

    for _, (hash_, d) in calibmap_id.items():
        calib_maps[hash_] = d

    return calib_maps


def find_calibration_maps(config, start, end, nickname, hashes=None):
    """Find all calibration maps within start/end corresponding to a single
    nickname."""
    reporter_factory = factories.ReporterFactory()
    calibratedir = names.tag2calibratedir(config.tag, rootdir=config.rootdir)
    calibratereporter = reporter_factory(
        calibratedir, start, end, **config.calibrate["reporting"]
    )

    calib_maps = {}
    for _, _, calib in calibratereporter.glob(nickname, start, end, data_id="*"):
        calib_maps[calib.hash] = calib

    # backwards compatibility with older (pre v0.5) maps
    if not calib_maps:
        name = f"{nickname}calibrate"
        for _, _, calib in calibratereporter.glob(name, start, end, data_id=None):
            calib_maps[calib.hash] = calib

    # do not reduce calibration maps without any ID metadata
    if any([calib.model_id for calib in calib_maps.values()]):
        calib_maps = reduce_maps_by_model_id(calib_maps)

    if hashes is not None:
        calib_maps = filter_data_by_hash(calib_maps, hashes)

    return calib_maps


def find_span_ids(all_series):
    """Find the span for all models and calibration maps used within timeseries."""
    models_segdict = {
        nickname: defaultdict(segmentlist) for nickname in all_series.keys()
    }
    calibs_segdict = {
        nickname: defaultdict(segmentlist) for nickname in all_series.keys()
    }
    for nickname, series in all_series.items():
        for s in series:
            seg = segment(s.start, s.end)
            models_segdict[nickname][names.hash2id(s.model_id)].append(seg)
            calibs_segdict[nickname][names.hash2id(s.calibration_id)].append(seg)
        for model in models_segdict[nickname].keys():
            models_segdict[nickname][model].coalesce()
        for calib in calibs_segdict[nickname].keys():
            calibs_segdict[nickname][calib].coalesce()

    return models_segdict, calibs_segdict


def find_target_features(start, end, config, segments, logger, n_events):
    # grab data
    dataloader_factory = factories.DataLoaderFactory()
    dataloader = dataloader_factory(start, end, segs=segments, **config.features)

    target_channel = config.samples["target_channel"]
    target_bounds = configparser.config2bounds(config.samples["target_bounds"])

    data_table = dataloader.query(channels=target_channel, bounds=target_bounds)
    data_table_ftrs = data_table.features[target_channel]
    snr_idxs = data_table_ftrs.argsort(keys=config.features["significance"])

    data_table_ftrs_srt = data_table_ftrs[snr_idxs]
    top_snr_triggers = data_table_ftrs_srt[-n_events:]
    return [row[0] for row in top_snr_triggers]


def find_loudest_times(nicknames, series, n_events):
    padding = 1.0
    loudest_times = {}
    for nickname in nicknames:
        loglike = []
        times = []
        for s in series[nickname]:
            loglike.extend(s.loglike.series)
            times.extend(s.times)

        loglike = np.array(loglike)
        times = np.array(times)
        unique_logl_idx = np.nonzero(loglike != np.roll(loglike, 1))[0]
        unique_logl = loglike[unique_logl_idx]
        unique_logl_times = times[unique_logl_idx]
        sort_idx = np.argsort(unique_logl)
        sorted_times = unique_logl_times[sort_idx]
        unique_times_bool = np.abs(sorted_times - np.roll(sorted_times, -1)) > padding
        unique_times_idx = np.nonzero(unique_times_bool)[0]
        unique_times = sorted_times[unique_times_idx]
        loudest_times[nickname] = unique_times[-n_events:]

    return loudest_times


def filter_data_by_hash(data, hashes):
    """Filter data products to only the identifiers (hashes) specified."""
    hashes = set(hashes)
    return {hash_: d for hash_, d in data.items() if hash_ in hashes}


def plot_path(name, instrument, tag, output_dir=".", suffix="png"):
    """Generate a standardized plot filename/path."""
    return os.path.join(
        output_dir, f"{name}-{instrument}-{__process_name__}-{tag}.{suffix}"
    )


def plot_histogram(
    config, start, end, nicknames, series, datasets=None, t0=None, output_dir="."
):
    """Create a histogram of the timeseries data and save it to disk"""
    if t0 is None:
        t0 = start

    fig = plots.histogram(nicknames, series, start, end, t0)
    fig.suptitle(
        f"{config.instrument} Timeseries Histogram\nwithin [{start:.3f}, {end:.3f})",
        fontsize=plots.DEFAULT_FONTSIZE,
    )

    path = plot_path(
        f"histogram-{'-'.join(nicknames)}-",
        config.instrument,
        config.tag,
        output_dir=output_dir,
    )
    plots.save(path, fig)

    return path


def plot_timeseries(
    config,
    start,
    end,
    nicknames,
    series,
    datasets=None,
    t0=None,
    segments=None,
    output_dir=".",
    plot_qscan=True,
):
    """Create a timeseries plot and saves it to disk."""
    if config.report.get("annotate_gch"):
        if not datasets:
            raise ValueError("datasets required when annotating glitches")
        gch_gps = []
        for dataset in datasets.values():
            gch_gps += list(dataset.vectors2classes()[0].times)
        gch_gps = sorted(set(gch_gps))
    else:
        gch_gps = None

    if t0 is None:
        t0 = start

    strain = None
    if plot_qscan:
        padding = 32
        target_channel = config.report.get(
            "qscan_channel", config.samples["target_channel"]
        )
        strain = TimeSeries.get(target_channel, start - padding, end + padding)

    fig = plots.timeseries(
        nicknames, series, start, end, t0, gch_gps=gch_gps, segs=segments, strain=strain
    )
    fig.suptitle(
        f"iDQ {config.instrument} Timeseries\nwithin [{start:.3f}, {end:.3f})",
        fontsize=plots.DEFAULT_FONTSIZE,
    )
    path = plot_path(
        f"timeseries-{'-'.join(nicknames)}-%s-%s" % (int(start), int(end)),
        config.instrument,
        config.tag,
        output_dir=output_dir,
    )
    plots.save(path, fig)
    return path


def plot_corner_glitch_clean(config, start, end, nicknames, datasets, output_dir="."):
    """Create a glitch/clean corner plot and saves it to disk."""
    # divide up classes
    gch = dict()
    cln = dict()
    for nickname in nicknames:
        gch[nickname], cln[nickname] = datasets[nickname].vectors2classes()

    # plot glitch correlations
    fig = plots.dataset_corner(nicknames, gch)
    fig.suptitle(
        f"{config.instrument} Glitch Correlations\nwithin [{start:.3f}, {end:.3f})",
        fontsize=plots.DEFAULT_FONTSIZE,
    )
    gch_path = plot_path(
        f"glitch_corner-{'-'.join(nicknames)}",
        config.instrument,
        config.tag,
        output_dir=output_dir,
    )
    plots.save(gch_path, fig)

    # plot clean correlations
    fig = plots.dataset_corner(nicknames, cln)
    fig.suptitle(
        f"{config.instrument} Clean Correlations\nwithin [{start:.3f}, {end:.3f})",
        fontsize=plots.DEFAULT_FONTSIZE,
    )
    cln_path = plot_path(
        f"clean_corner-{'-'.join(nicknames)}",
        config.instrument,
        config.tag,
        output_dir=output_dir,
    )
    plots.save(cln_path, fig)

    return gch_path, cln_path


def plot_calibration_coverage(
    config,
    start,
    end,
    nicknames,
    series,
    datasets,
    calibmaps,
    output_dir=".",
    calib_id=None,
    plot_map_coverage=False,
):
    """Create a calibration coverage plot and saves it to disk."""
    fig = plots.calibration_coverage(
        nicknames,
        series,
        datasets,
        calibmaps,
        legend=config.report.get("legend", False),
        plot_map_coverage=plot_map_coverage,
    )
    fig.suptitle(
        f"{config.instrument} Calibration Coverage\nwithin [{start:.3f}, {end:.3f})",
        fontsize=plots.DEFAULT_FONTSIZE,
    )
    path = plot_path(
        f"calibration_coverage-{'-'.join(nicknames)}",
        config.instrument,
        config.tag,
        output_dir=output_dir,
    )
    plots.save(path, fig)
    return path


def plot_calibration_accuracy(
    config,
    start,
    end,
    nicknames,
    calibmaps,
    series,
    datasets,
    output_dir=".",
    calib_id=None,
    plot_map_coverage=False,
):
    """Create a calibration accuracy plot and saves it to disk."""
    fig = plots.calibration_accuracy(
        nicknames,
        calibmaps,
        series,
        datasets,
        legend=config.report.get("legend", False),
        plot_map_coverage=plot_map_coverage,
    )
    fig.suptitle(
        f"{config.instrument} Calibration Accuracy\nwithin [{start:.3f}, {end:.3f})",
        fontsize=plots.DEFAULT_FONTSIZE,
    )
    path = plot_path(
        f"calibration_accuracy-{'-'.join(nicknames)}",
        config.instrument,
        config.tag,
        output_dir=output_dir,
    )
    plots.save(path, fig)
    return path


def plot_calibration_pdf_cdf(
    config, start, end, nicknames, calibmaps, output_dir=".", calib_id=None
):
    """Create a PDF/CDF plot for glitch/clean distributions and saves it to
    disk."""
    fig = plots.calibration_distribs(nicknames, calibmaps)
    fig.suptitle(
        f"{config.instrument} Calibration Distributions\n"
        f"within [{start:.3f}, {end:.3f})",
        fontsize=plots.DEFAULT_FONTSIZE,
    )
    path = plot_path(
        f"calibration_distributions-{'-'.join(nicknames)}",
        config.instrument,
        config.tag,
        output_dir=output_dir,
    )
    plots.save(path, fig)
    return path


def plot_roc(
    config,
    start,
    end,
    nicknames,
    datasets,
    series=None,
    output_dir=".",
    annotate_auc=False,
    rank_roc=False,
):
    """Create a ROC curve plot and saves it to disk."""
    fig = plots.roc(
        nicknames, series, datasets, annotate_auc=annotate_auc, rank_roc=rank_roc
    )
    fig.suptitle(
        f"{config.instrument} Receiver Operating Characteristics\n"
        f"within [{start:.3f}, {end:.3f})",
        fontsize=plots.DEFAULT_FONTSIZE,
    )
    path = plot_path(
        f"roc-{'-'.join(nicknames)}",
        config.instrument,
        config.tag,
        output_dir=output_dir,
    )
    plots.save(path, fig)
    return path


def plot_feature_importance(
    config,
    start,
    end,
    nicknames,
    models,
    datasets=None,
    series=None,
    t0=None,
    output_dir=".",
):
    """Create a feature importance plot and saves it to disk."""
    if series:
        segdict, _ = find_span_ids(series)
    else:
        segdict = None

    # instantiate classifiers to pick up some standard stuff...
    classifier_factory = factories.ClassifierFactory()
    klassifiers = {
        nickname: classifier_factory(
            nickname, rootdir=output_dir, **config.classifier_map[nickname]
        )
        for nickname in nicknames
    }

    figs, tabs = plots.featureimportance(
        nicknames, models, klassifiers, datasets, start, end, t0=t0, segdict=segdict
    )
    paths = []
    cols_datas = []
    for nickname in nicknames:
        for key, model in models[nickname].items():
            model_name = f"{nickname}_{model.hash}"

            try:
                path = plot_path(
                    f"dq_flags-{nickname}-{model.hash}",
                    config.instrument,
                    config.tag,
                    output_dir=output_dir,
                    suffix="hdf5",
                )
                working_data = datasets[nickname].copy()
                working_data.start = start
                working_data.end = end
                working_data.filter()
                model.save_as_data_quality_flags(
                    path,
                    working_data,
                    klassifiers[nickname].time,
                    klassifiers[nickname].significance,
                )
            except Exception:
                pass

            fig = figs[nickname][key]
            tab = tabs[nickname][key]
            if fig is not None:
                path = plot_path(
                    f"featureimportance-{nickname}-{model.hash}",
                    config.instrument,
                    config.tag,
                    output_dir=output_dir,
                )
                plots.save(path, fig)
                paths.append((model_name, path))
            else:
                paths.append((model_name, None))

            if tab is not None:
                cols, data = tab
                cols_datas.append((model_name, cols, data))
            else:
                cols_datas.append((model_name, None, None))

    return paths, cols_datas
